/*
* @file Engine.cpp
* @author Mukrime_Sagirolu_152120191034
* @author Hazar_Namdar_152120191053
*/

#include "Engine.h"

Engine* Engine::instance = 0;
/**
* @brief Yapici fonksiyon tanimlandi.
*/
Engine::Engine() :fuel_per_second(5.5), status(true), temperature(40) {
	InternalTank internal(this->id, 55.0, 0, false);
	this->tank = internal;
	this->id++;
}
/**
* @brief Yikici fonksiyon tanimlandi.
*/
Engine::~Engine() {
	cout << "Engine: Simulation stopped";
}

/**
* @brief Singleton nesnesi yaratildi.
*/
Engine *Engine::getInstance()
{
	if (instance == 0)
		instance = new Engine();
	
	return instance;
}
/**
* @brief Motor calistirildi.
*/
bool Engine::start_engine() {
	if (status)
		return status;
}
/**
* @brief Motor durduruldu.
*/
bool Engine::stop_engine() {
	if (!status)
		return status;
}

/**
* @brief Tank, motora baglandi.
*/
void Engine::connect_fuel_tank_to_engine(int fuelTankId) {
	FuelTankList* list = FuelTankList::getInstance();

	for (int i = 0; i < list->getList().size(); i++) {
		if (list->getList().at(i).getId() == fuelTankId) {
			if (!list->getList().at(i).getValve().getStatus()) {
				this->getTank().getValve().setStatus(false);
				list->open_valve(list->getList().at(i).getId());
				this->getTank() = list->getList().at(i);
			}
		}
	}
}

/**
* @brief Tank motordan ayrildi.
*/
void Engine::disconnect_fuel_tank_to_engine(int fuelTankId) {
	Engine::stop_engine();

	if (this->getTank().getId() == fuelTankId) {
		this->getTank().getValve().setStatus(false);
	}
}

/**
* @brief Getter metodu tanimlandi.
*/
double Engine::getFuelPerSecond() {
	return this->fuel_per_second;
}

/**
* @brief Motor tamir edildi.
*/
void Engine::repairEngine() {
	if (this->health != 0) {
		this->health = 100;
	}
}

/**
* @brief Motor tam gaz calisti.
*/
void Engine::fullThrottle() {
	this->fuel_per_second = this->fuel_per_second * 5;
	if (this->temperature < 90) {
		this->health -= 1;
	}
	this->temperature += 5;
}

/**
* @brief Setter metodu tanimlandi.
*/
void Engine::setFuelPerSec(double fuelpersec) {
	this->fuel_per_second = fuelpersec;
}

/**
* @brief Setter metodu tanimlandi.
*/
void Engine::setHealth(int health) {
	this->health = health;
}

/**
* @brief Motor blogu dešistirildi.
*/
void Engine::changeEngineBlock() {
	if (this->health == 0) {
		this->fuel_per_second = 5.5;
		this->health = 100;
		this->status = true;
		this->temperature = 40;
	}
}

/**
* @brief Setter metodu tanimlandi.
*/
void Engine::setTemperature(int temperature) {
	this->temperature = temperature;
}

/**
* @brief Getter metodu tanimlandi.
*/
FuelTank Engine::getTank() {
	return this->tank;
}

/**
* @brief Getter metodu tanimlandi.
*/
int Engine::getTemperature() {
	return this->temperature;
}

/**
* @brief Getter metodu tanimlandi.
*/
int Engine::getHealth() {
	return this->health;
}
