/*
* @file Engine.h
* @author Mukrime_Sagiroglu_152120191034
* @author Hazar_Namdar_152120191053
*/

#include <iostream>
#include <stdlib.h>
#include "FuelTank.h"
#include "InternalTank.h"
#include "Observer.h"

using namespace std;

#pragma once
/**
* @brief class olusturuldu ve miras alindi.
*/
class Engine :public Observer{
private:
/**
* @brief ozellikler olusturuldu.
*/
	static Engine* instance;
	int id = 0;
	bool status;
	double fuel_per_second;
	int temperature;
	int health = 100;
	FuelTank tank;
public:
/**
* @brief fonsiyonlar olusturuldu.
*/
	Engine();
	~Engine();
	static Engine* getInstance();
	int getTemperature();
	int getHealth();
	double getFuelPerSecond();
	bool start_engine();
	bool stop_engine();
	void add_fuel(int, double);
	void connect_fuel_tank_to_engine(int);
	void disconnect_fuel_tank_to_engine(int);
	void repairEngine();
	void fullThrottle();
	void setFuelPerSec(double);
	void setHealth(int);
	void changeEngineBlock();
	void setTemperature(int);
	FuelTank getTank();
};


