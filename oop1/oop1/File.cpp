/*
* @file File.cpp
* @author Bulent Bugrahan DALLI - 152120161035
* @author Feyza Atesoglu -152120181104
* @author M�krime_Sagiroglu_152120191034
* @author Hazar_Namdar_152120191053
*/
#include "File.h"
#include <string>
#include <sstream>

/**
* @brief Class i�inde kullanilmasi icin bir fonksiyon olusturuldu.
*/
string getValuesFromOutputFile(string);

/**
* @brief txt dosyasindaki verileri okuyan fonksiyon tanimlandi.
*/
void File::is_command(fstream& input) {
	string str;
	int tankId, nextSec = 0, waitSec = 0;
	bool idle = false;
	double totalQuantity = 0;
	ofstream output("output.txt");

/**
* @brief Dosyanin acilip acilmama durumu kontrol edildi.
*/
	try {
		string msg;
		if (!input.is_open()) {
			msg = "File doesn't exist or couldn't open";
			throw msg;
		}
	}
	catch (string msg) {
		cout << msg << endl;
		exit(0);
	}

/**
* @brief singleton nesneleri yaratildi.
*/
	FuelTankList* list = FuelTankList::getInstance();
	Engine *engine=Engine::getInstance();

/**
* @brief Dosyanin bos olup olmadigi kontrol edildi.
*/
	if (input.peek() == input.eof()) {
		output << "File is empty" << endl;
		system("pause");
	}

/**
* @brief Her komut icin bir kosul durumu olusturuldu ve gerekli islemler yapildi.
*/
	while (input >> str) {
		if (list->getSec() == nextSec) {
			engine->setFuelPerSec(5.5);
		}
		if (engine->getTemperature() > 130) {
			engine->setHealth(engine->getHealth() - 1);
		}
		
		if (engine->getTemperature() < 20 || engine->getHealth() <= 0) {
			engine->stop_engine();
		}

		if (list->getSec() == waitSec) {
			idle = false;
		} else {
			idle = true;
			if (engine->getTemperature() < 90) {
				engine->setTemperature(engine->getTemperature() + 1);
			} else if (engine->getTemperature() > 90) {
				engine->setTemperature(engine->getTemperature() - 1);
			}
		}
		
		if (str == "start_engine;") {
			if (engine->getTemperature() > 20 && engine->getHealth() > 0) {
				output << "Engine starts." << endl;
				engine->start_engine();
			} else {
				output << "Engine stops. (Temperature or Health is not ok for starts.)";
			}

		} else if (str == "stop_engine;") {
			output << "Engine stops." << endl;
			engine->stop_engine();

		} else if (str == "print_fuel_tank_count") {
			output << "Fuel Tank Count : " << list->getList().size();

		} else if (str == "change_engine_block") {
			if (!engine->start_engine()) {
				engine->changeEngineBlock();
			}

		} else if (str == "repair_engine") {
			if (!engine->start_engine()) {
				engine->repairEngine();
			}

		} else if (str == "list_connected_tanks") {

		} else if (str == "print_total_fuel_quantity;") {
			output << "Total Fuel Quantity : ";

			for (int i = 0; i < list->getList().size(); i++) {
				totalQuantity += list->getList().at(i).getFuelQuantity();
			}

			output << totalQuantity << endl;

		} else if (str == "list_connected_tanks;") {
			output << "Command does not exist. -- (List connected tanks still in progress)" << endl;

		} else if (str == "print_total_consumed_fuel_quantity;") {
			output << "Command does not exist. -- (Print total consumed fuel quantity still in progress)" << endl;

		} else if (str == "print_fuel_tank_count;") {

			output << "Total Fuel Tank Count : " << list->getList().size() << endl;

		} else if (str == "wait") {
			input >> str;
			waitSec = stoi(getValuesFromOutputFile(str));
			waitSec += list->getSec();

		} else if (str == "print_tank_info") {
			input >> str;
			int tankId = stoi(getValuesFromOutputFile(str));
			output << endl;
			for (int i = 0; i < list->getList().size(); i++) {
				if (list->getList().at(i).getId() == tankId) {
					output << "ID : " << tankId << endl <<
						"Capacity : " << list->getList().at(i).getCapacity() << endl <<
						"Fuel Quantity : " << list->getList().at(i).getFuelQuantity() << endl <<
						"Broken status : " << list->getList().at(i).getStatus() << endl<<endl;
				}
			}

		} else if (str == "fill_tank") {
			input >> str;
			int tankId = stoi(getValuesFromOutputFile(str));

			input >> str;
			double fuelQuantity = stoi(getValuesFromOutputFile(str));

			for (int i = 0; i < list->getList().size(); i++) {
				if (list->getList().at(i).getId() == tankId) {
					list->getList().at(i).addFuelQuantity(fuelQuantity);
				}
			}

		} else if (str == "add_fuel") {
			output << "Adding Fuel : ";
			input >> str;
			int tankId = stoi(getValuesFromOutputFile(str));
			output << str << " ";
			input >> str;
			double quantity = stoi(getValuesFromOutputFile(str));
			output << str << endl;
			list->add_fuel(tankId, quantity);

		} else if (str == "add_fuel_tank") {
			output << "Adding Tank : ";
			input >> str;
			double capacity = stoi(getValuesFromOutputFile(str));
			list->add_fuel_tank(capacity);
			output << str << endl;

		} else if (str == "list_fuel_tanks;") {
			output << "Fuel Tanks List: " << endl;
			output << endl;
			output << list->list_fuel_tanks().str();

		} else if (str == "full_throttle") {
			input >> str;
			int sec = stoi(getValuesFromOutputFile(str));
			nextSec = list->getSec() + sec;
			engine->fullThrottle();

		} else if (str == "remove_fuel_tank") {
			output << "Remove Fuel Tank : ";
			input >> str;
			int tankId = stoi(getValuesFromOutputFile(str));
			list->remove_fuel_tank(tankId);
			output << str << endl;
		
		
		} else if (str == "connect_fuel_tank_to_engine") {
			output << "Connect fuel tank from engine : ";
			input >> str;
			int tankId = stoi(getValuesFromOutputFile(str));
			engine->connect_fuel_tank_to_engine(tankId);
			output << str << endl;

		} else if (str == "disconnect_fuel_tank_from_engine") {
			output << "Disconnect fuel tank from engine : ";
			input >> str;
			int tankId = stoi(getValuesFromOutputFile(str));
			engine->disconnect_fuel_tank_to_engine(tankId);
			output << str << endl;

		} else if (str == "open_valve") {
			output << "Open valve : ";
			input >> str;
			int tankId = stoi(getValuesFromOutputFile(str));
			list->open_valve(tankId);
			output << str << endl;

		} else if (str == "close_valve") {
			output << "Close valve : ";
			input >> str;
			int tankId = stoi(getValuesFromOutputFile(str));
			list->close_valve(tankId);
			output << str << endl;

		} else if (str == "break_fuel_tank") {
			output << "Break fuel tank : ";
			input >> str;
			int tankId = stoi(getValuesFromOutputFile(str));
			list->breakTank(tankId);
			output << str << endl;

		} else if (str == "repair_fuel_tank") {
			output << "Fuel tank repaired : ";
			input >> str;
			int tankId = stoi(getValuesFromOutputFile(str));
			list->repairTank(tankId);
			output << str << endl;

		} else if (str == "stop_simulation;") {
			output << "End of the simulation." << endl;
			exit(0);

		} else {
			output << "Command does not exist." << endl;
		}
		list->incrementSec();
	}

}

/**
* @brief Class icinde kullan�lacak fonksiyon tanimlandi.
*/
string getValuesFromOutputFile(string str) {
	string value;
	for (int i = 0; i < str.length(); i++) {
		if (str.at(i) == '<' || str.at(i) == '>' || str.at(i) == ';') {
			continue;
		}
		value += str.at(i);
	}

	return value;
}


