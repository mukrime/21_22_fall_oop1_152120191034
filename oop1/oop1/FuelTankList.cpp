/**
* @file					FuelTankList.cpp
* @Author				Bulent Bugrahan DALLI ----> bugrahandalli@msn.com
* @date					Ocak, 2021
* @brief				FuelTankList sinifinin icrasinin yapildigi kod blogudur.
*/

#include "FuelTankList.h"
FuelTankList* FuelTankList::instance = 0;
int FuelTankList::id = 0;
/**
* @brief				Static objeyi donduren methoddur.
*/
FuelTankList* FuelTankList::getInstance() {
	if (instance == 0) {
		instance = new FuelTankList();
	}
	return instance;
}
/**
* @brief				Static objenin icinde olan, listeyi(vector) donduren methoddur.
*/
vector<FuelTank> &FuelTankList::getList() {
	return this->vec;
}
/**
* @brief				Listeye yeni tank eklemek icin kullanilan methoddur.
* @param newFuelTank	Listeye eklenecek olan tankin degerinin tutuldugu degiskendir.
*/
void FuelTankList::addList(FuelTank newFuelTank) {
	this->vec.push_back(newFuelTank);
}
/**
* @brief				Objeye ait olan sureyi donduren methoddur.
*/
int FuelTankList::getSec() {
	return this->sec;
}
/**
* @brief				Objeye ait olan sureyi 1 arttiran methoddur.
*/
void FuelTankList::incrementSec() {
	this->sec++;
}
/**
* @brief				Listeye yeni tank eklemek icin kullanilan methoddur.
* @param capacity	Listeye eklenecek olan tankin kapasite degerinin tutuldugu degiskendir.
*/
void FuelTankList::add_fuel_tank(double capacity) {
	FuelTank temp(this->id, capacity, 0, false);
	this->vec.push_back(temp);
	this->id++;
}
/**
* @brief				Listeden tanki cikartmak icin kullanilan methoddur.
* @param id				Listeden cikartilacak tankin id degerinin tutuldugu degiskendir.
*/
void FuelTankList::remove_fuel_tank(int id) {
	if (id > this->vec.size()) {
		cout << "The ID of remove tank does not exist.";//If id doesnt exist, remove fuel tank method wont work.
		return;
	}
	for (int i = 0; i < this->vec.size(); i++) {
		if (this->vec.at(i).getId() == id) {
			this->vec.erase(this->vec.begin() + i);
			break;
		}
	}
}
/**
* @brief				Listedeki belirtilen tanka yakit eklemek icin kullanilan methoddur.
* @param id				Listeki yakit eklenecek olan tankin id degerinin tutuldugu degiskendir.
* @param fuel_quantity	Eklenecek yakit miktarinin degerinin tutuldugu degiskendir.
*/
void FuelTankList::add_fuel(int id, double fuel_quantity) {
	try {
		bool check = false;
		for (int i = 0; i < this->vec.size(); i++) {
			if (this->vec.at(i).getId() == id) {
				if ((this->vec.at(i).getFuelQuantity() + fuel_quantity) > this->vec.at(i).getCapacity()) {
					this->vec.at(i).setFuelQuantity(this->vec.at(i).getCapacity());
				}
				else {
					this->vec.at(i).addFuelQuantity(fuel_quantity);
				}
				check = true;
				break;
			}
		}
		if (!check) {
			throw (string)"Id does not exist.(add_fuel)\n";//If id doesnt exist, add fuel method wont work.
		}
	}
	catch (string str) {
		cout << "Exception:" << str << endl;
	}
}
/**
* @brief				Listedeki belirtilen tanki bozma islemini uygulayan methodun tanimi.
* @param id				Listeki bozulacak olan tankin id degerinin tutuldugu degiskendir.
*/
void FuelTankList::breakTank(int id) {
	for (int i = 0; i < this->vec.size(); i++) {
		if (this->vec.at(i).getId() == id) {
			this->vec.at(i).setStatus(false);
			break;
		}
	}

}
/**
* @brief				Listedeki belirtilen tanki tamir islemini uygulayan methodun tanimi.
* @param id				Listeki tamir edilecek olan tankin id degerinin tutuldugu degiskendir.
*/
void FuelTankList::repairTank(int id) {
	for (int i = 0; i < this->vec.size(); i++) {
		if (this->vec.at(i).getId() == id) {
			this->vec.at(i).setStatus(true);
			break;
		}
	}
}
/**
* @brief				Listedeki belirtilen tankin kapaginin acilmasi islemini uygulayan methodun tanimi.
* @param id				Listeki kapaginin acilmasi istenen tankin id degerinin tutuldugu degiskendir.
*/
void FuelTankList::open_valve(int id) {
	for (int i = 0; i < this->vec.size(); i++)
	{
		if (this->vec.at(i).getId() == id)
			this->vec.at(i).getValve().setStatus(true);
	}
}
/**
* @brief				Listedeki belirtilen tankin kapaginin kapanmasi islemini uygulayan methodun tanimi.
* @param id				Listeki kapaginin kapanmasi istenen tankin id degerinin tutuldugu degiskendir.
*/
void FuelTankList::close_valve(int id) {
	for (int i = 0; i < this->vec.size(); i++)
	{
		if (this->vec.at(i).getId() == id)
			this->vec.at(i).getValve().setStatus(false);
	}
}
/**
* @brief				Listedeki tanklarin degerlerini(id, kapasite, yakit miktari, bozuk olma durumu) ekrana yazdiran methodun tanimi.
*/
stringstream FuelTankList::list_fuel_tanks() {
	stringstream ss;
	for (int i = 0; i < this->vec.size(); i++) {

		ss << i << ". FuelTank: " << endl <<
			"ID:" << this->vec.at(i).getId() << endl <<
			"Capacity:" << this->vec.at(i).getCapacity() << endl <<
			"Fuel Quantity:" << this->vec.at(i).getFuelQuantity() << endl <<
			"Status:" << this->vec.at(i).getStatus() << endl << endl;
	}

	return ss;
}

