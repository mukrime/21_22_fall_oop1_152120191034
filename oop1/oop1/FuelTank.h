/*
* @file FuelTank.h
* @author Bulent Bugrahan DALLI - 152120161035
* @author Feyza Atesoglu - 152120181104
*/

#include <iostream>
#include <stdlib.h>
#include <vector>
#include "FuelTankList.h"
#include "Valve.h"
#include "Observer.h"
using namespace std;

#ifndef FuelTank_H
#define FuelTank_H
/**
* @brief class olusturuldu ve miras alindi.
*/
class FuelTank: public Observer {
protected:
/**
* @brief ozellikler olusturuldu.
*/
    int id;
    double capacity;
    double fuel_quantity;
    bool broken;
	Valve valve;
public:
/**
* @brief fonsiyonlar olusturuldu.
*/
    FuelTank();
	~FuelTank();
    FuelTank(int, double, double, bool);
    FuelTank(const FuelTank&);
    int getId();
    double getCapacity();
    double getFuelQuantity();
    bool getStatus();
    void setStatus(bool);
	Valve getValve();
    void addFuelQuantity(double);
    void fillTank(int, double);
    void setFuelQuantity(double);
    FuelTank add(FuelTank& tank1, FuelTank& tank2);
	//FuelTank operator+(FuelTank&);
	//FuelTank operator=(const FuelTank&);
};
#endif
