/*
* @file FuelTank.cpp
* @author Bulent Bugrahan DALLI - 152120161035
* @author Feyza Atesoglu - 152120181104
*/
#include "FuelTank.h"

/**
* @brief Yapici fonksiyonlar tanimlandi.
*/
FuelTank::FuelTank() {}

FuelTank::FuelTank(int id, double capacity, double fuel_quantity, bool broke) {
	this->id = id;
	this->capacity = capacity;
	this->fuel_quantity = fuel_quantity;
	this->broken = broke;
}


FuelTank::FuelTank(const FuelTank& tank1) {
	this->id = tank1.id;
	this->capacity = tank1.capacity;
	this->fuel_quantity = tank1.fuel_quantity;
	this->broken = tank1.broken;
}

/**
* @brief Yikici fonksiyon tanimlandi.
*/
FuelTank::~FuelTank()
{
	cout << "Tank " << this->id << ": Simulation stopped\n";
}

/**
* @brief Getter metodu eklendi.
*/
int FuelTank::getId() {
	return this->id;
}

/**
* @brief Getter metodu eklendi.
*/
double FuelTank::getCapacity() {
	return this->capacity;
}

/**
* @brief Getter metodu eklendi.
*/
double FuelTank::getFuelQuantity() {
	return this->fuel_quantity;
}

/**
* @brief Getter metodu eklendi.
*/
bool FuelTank::getStatus() {
	return this->broken;
}

/**
* @brief Setter metodu eklendi.
*/
void FuelTank::setStatus(bool status) {
	this->broken = status;
}

/**
* @brief Getter metodu eklendi.
*/
Valve FuelTank::getValve()
{
	return this->valve;
}

/**
* @brief Yakit eklendi.
*/
void FuelTank::addFuelQuantity(double quantity) {
	if (quantity < 0) {
		quantity = 0;
	}
	this->fuel_quantity = this->fuel_quantity + quantity;
}

/**
* @brief Depo dolduruldu.
*/
void FuelTank::fillTank(int id, double quantity) {

	FuelTankList* list = FuelTankList::getInstance();

	for (int i = 0; i < list->getList().size(); i++) {
		if (list->getList().at(i).getId() == id) {
			if (list->getList().at(i).getCapacity() < quantity) {
				list->getList().at(i).setFuelQuantity(list->getList().at(i).getCapacity());
				return;
			}

			list->getList().at(i).addFuelQuantity(quantity);
		}
	}
}

/**
* @brief Setter metodu eklendi.
*/
void FuelTank::setFuelQuantity(double fuel_quantity) {
	this->fuel_quantity = fuel_quantity;
}

/**
* @brief Iki tank toplandi.
*/
FuelTank FuelTank::add(FuelTank& tank1, FuelTank& tank2) {
	if ((this->capacity - this->fuel_quantity) > (tank1.capacity + tank2.capacity)) {
		this->fuel_quantity = this->fuel_quantity + tank1.fuel_quantity + tank2.fuel_quantity;
		tank1.fuel_quantity = 0;
		tank2.fuel_quantity = 0;
	}

	return *this;
}

/*FuelTank FuelTank::operator+(FuelTank& tank) {
	FuelTank returnTank = *this;
	FuelTank returnTank2 = tank;

	if (!returnTank.getStatus() && !tank.broken) {
		if (returnTank.getCapacity() > tank.capacity) {
			if ((returnTank.getCapacity() - returnTank.getFuelQuantity()) > (tank.fuel_quantity)) {
				returnTank.fuel_quantity = returnTank.fuel_quantity + tank.fuel_quantity;
				tank.fuel_quantity = 0;
				this->fuel_quantity = 0;
				return returnTank;
			} 			else {
				cout << "Fuel tank have NOT enough empty space in it.\n";
			}
		} 		else {
			if ((returnTank2.capacity - returnTank2.fuel_quantity) > this->fuel_quantity) {
				returnTank2.fuel_quantity = this->fuel_quantity + returnTank2.fuel_quantity;
				tank.fuel_quantity = 0;
				this->fuel_quantity = 0;
				return returnTank2;
			} 			else {
				cout << "Fuel tank have NOT enough empty space in it.\n";
			}
		}
	}
}

FuelTank FuelTank::operator=(const FuelTank& tank) {
	if (this->getCapacity() - this->getFuelQuantity() > tank.fuel_quantity) {
		this->fuel_quantity = this->fuel_quantity + tank.fuel_quantity;
	}
	return *this;
}*/