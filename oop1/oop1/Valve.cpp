#include "Valve.h"
#include <iostream>
/**
* @brief Yapici fonksiyon tanimlandi.
*/
Valve::Valve() {
	this->status = false;
}
/**
* @brief Yikici fonksiyon tanimlandi.
*/
Valve::~Valve() {
	std::cout << "Valve: Simulation stopped" << std::endl;
}
/**
* @brief Getter metodu eklendi.
*/
bool Valve::getStatus() {
	return this->status;
}
/**
* @brief Setter metodu eklendi.
*/
void Valve::setStatus(bool status) {
	this->status = status;
}
