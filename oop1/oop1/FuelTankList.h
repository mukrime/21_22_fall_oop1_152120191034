/**
* @file					FuelTankList.h
* @Author				Bulent Bugrahan DALLI ----> bugrahandalli@msn.com
* @date					Ocak, 2021
* @brief				FuelTankList sinifinin uye ve methodlarinin tanimlandigi kod blogudur.
*/
#pragma once
#include <vector>
#include <sstream>
#include "FuelTank.h"
/**
* @brief				Sinifin genel tanimlamasi, methodlarin tanimi yapilir.
* @param instance		Static olan instance objesinin degerini tutar.
* @param id				Listedeki objeye ait id degerini tutar.
* @param vec			Static objenindeki liste degerlerini tutar.
* @param sec			Sure degiskenini tutar.
*/
class FuelTankList {
private:
	static FuelTankList* instance;
	static int id;
	vector <FuelTank> vec;
	int sec = 0;
public:
	//! Instance'in getter methodunun tanimi
	static FuelTankList* getInstance();
	//! Vector uyesinin getter tanimi
	vector<FuelTank> &getList();
	//! Vector uyesine add islemini yapan methodun tanimi
	void addList(FuelTank);
	//! Sureyi arttiran methodun tanimi
	void incrementSec();
	//! Sure degiskeninin getter methodunun tanimi
	int getSec();
	//! Listeye yeni tank eklenmesi icin kullanilan methodun tanimi
	void add_fuel_tank(double);
	//! Listedeki tank'a fuel eklenmesi icin kullanilan methodun tanimi
	void add_fuel(int, double);
	//! Listedeki tank'i bozan methodun tanimi
	void breakTank(int);
	//! Listedeki tank'i tamir eden methodun tanimi
	void repairTank(int);
	//! Listedeki tank'in kapagini acan methodun tanimi
	void open_valve(int);
	//! Listedeki tank'in kapagini kapatan methodun tanimi
	void close_valve(int);
	//! Listedeki tanklarin degerlerini ekrana yazdiran methodun tanimi
	stringstream list_fuel_tanks();
	//! Listedeki tank'i listeden cikartan methodun tanimi
	void remove_fuel_tank(int);
};

